package PDLx::Role::RestrictedPDL;

# ABSTRACT: restrict write access to a PDL object as much as possible

use strict;
use warnings;

use PDL::Core ':Internal';
use overload;

our $VERSION = '0.07';

my $_cant_mutate;

BEGIN {
    $_cant_mutate = sub {
        require Carp;
        Carp::croak "restricted piddle: cannot mutate";
    };
}


use Moo::Role;
use namespace::clean 0.16;

use overload
  map  { $_ => $_cant_mutate }
  grep { $_ =~ /=$/ }
  map  { split( ' ', $_ ) } @{overload::ops}{ 'assign', 'mutators', 'binary' };

sub is_inplace  { 0; }
sub set_inplace { goto $_cant_mutate unless @_ > 0 && $_[1] == 0 }

sub inplace;
*inplace = $_cant_mutate;

1;

# COPYRIGHT

__END__


=for Pod::Coverage
inplace
is_inplace
PDL
set_inplace

=for stopwords
PDL

=pod

=head1 SYNOPSIS

  package MyPDL;

  use Moo;

  with 'PDLx::Role::RestrictedPDL';


=head1 DESCRIPTION

This role overloads assignment operators and the
L<< B<is_inplace>|PDL::Core/inplace >>,
L<< B<is_inplace>|PDL::Core/is_inplace >>,
and
L<< B<set_inplace>|PDL::Core/set_inplace >>,
methods to attempt to prevent mutation of a piddle.

=cut
